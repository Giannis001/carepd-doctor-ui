function ajax_request(Http_method,link,data,callback){
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function(){
        if (xhttp.readyState === 4 && xhttp.status ===200 ){
            console.log(xhttp.responseText);
            callback(xhttp.responseText);
        }
    };
    xhttp.open(Http_method,link,true);
    xhttp.send(data);
}

function AddMainPage(response){
    $('body').empty();
    $('body').append(
    '<div class="container-fluid"id="MainPage">'

    +'<div class="row justify-content-center">'
    +   '<div class="col-12">'
    +       '<header class="mainHeader">CMR Project</header>'
    +   '</div>'
   +'</div>'
   +'<div class="row justify-content-center">'
    +    '<div class="col-2 topBtn">'
     +      '<button onclick="LoadSettings()" class="btn topBtn">Settings</button>'
      + '</div>'
      + '<div class="col-8 ">'
     +    '<p class="welcome">Welcome Dr. Name</p>'
  +     '</div>'
  +         '<div class="col-2 topBtn">'
  +             '<button onclick= "LoadLogIn()" class="btn topBtn">Log Out</button>'
  +         '</div>'
  + '</div>'
  +     '<div class="row justify-content-center">'
  +         '<div class="col-8">'
  +             '<button onclick= "LoadPatients()" class="btn patientsBtn"> Patients</button>'
  +         '</div>'
  +     '<div class="col-8">'
  +        '<button onclick="LoadStatistics()" class="btn statBtn">Statistics</button>'
  +     '</div>'
  +     '</div>'
  + '</div>');
}
function SendSignin(){
    
    function callback(response){
            AddMainPage(response);//Load Data of doctor.
    }
    callback(); //use this in order to load the next page automatically,if sign in data exchange works then remove.
    var data = {"username" : $('#username').val() ,"password"  : $('#password').val()};
    ajax_request(POST,'Where to send - Add.',data,callback);
}
