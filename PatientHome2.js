(function(){
    newPatient = new Vue({
        el: "#patientList",
        data: {
            patients:[
                {	
					id: '',
                    image:'profileIcon.png',
                    name: '',
                    surname: '',
                    gender: '',
                    age: ''
                }
            ]
        },
        methods: {
            addNewPatient(id, name, surname, gender, age, image){
                this.patients.push({
					id: id,
                    image: image,
                    name: name,
                    surname: surname,
                    gender: gender,
                    age: age
                })
            },
            gereratePatients(){
                for(let i = 1; i < 32; i++){
                    var image;
                    if(i%2 == 0){
                        sex = "Female";
                        this.image='profileIcon2.png'
                    }else{
                        sex = "Male";
                        this.image='profileIcon.png'
                    }
                    this.addNewPatient(i, "dummy"+i, "dummy"+i, sex, 50+i, this.image);
                }
            }
        },
        
    })
    
}());

function getNumberOfPatients(patients){
	//TODO return the number of patients.
	console.log(patients);
	genderHash = {
		"m" : "male",
		"f" : "female"
	}
	var patientsListHTML;
	patientsListHTML = '';
	for(i=0;i<patients.length;i++){ //TODO replace 32 with patientsNumber when callback is ready.
		patientsListHTML += '<ul id="patientList">'
+								'<form id="info">'
+                				  '<button onclick="LoadCalendar('+patients[i].user_id+')" id="p0" class="patient">'
+									  '<li class="col-12" id="column">'
+                   					 '<label class="lab" for="nameField">Name</label>'
+                                        '<input type="text" class="field" id="nameField" value="'+ patients[i].name +'" readonly>'
+                                        '<hr>'
+                                        '<label class="lab" for="surnameField">Surname</label>'
+                                        '<input type="text" class="field" id="surnameField" value="'+ patients[i].surname +'" readonly>'
+                                        '<hr>'
+                                        '<label class="lab" for="genderField">Gender</label>'
+                                        '<input type="text" class="field" id="genderField" value="'+ genderHash[patients[i].gender] +'" readonly>'
+                                        '<hr>'
+                                        '<label class="lab" for="ageField">Age</label>'
+                                        '<input type="text" class="field" id="ageField" value="'+ patients[i].age +'" readonly>'
+				    				   '</li>'  
+                       		   '</button>'       
+                   			 '</form>'
+							 '</ul>'
	}

    $('body').empty();
    $('body').append(

'<div class="container-fluid"id="MainPage">'
+            '<div class="row justify-content-center my-5">'
+               '<div class="col-2 topBtn">'
+                   '<button onclick="GoBackFromPatient()" class="btn topBtn">Back</button>'
+                '</div>'
+                '<div class="col-8 header">'
+                    '<header>Patients</header>'
+                '</div>'
+                '<div class="col-2 topBtn">'
+                    '<button onclick="GoToHome()" class="btn topBtn">Home</button>'
+                '</div>'
+            '</div>'
+			 '<div class="row justify-content-center">'
+               patientsListHTML                                
+            '</div>'
+        '</div>'

);

}

function LoadPatientsPage(PatientsData){
	
    newPatient.gereratePatients();
	var doctorName = "fanou"; //TODO after login implementation replace "Doctor1" with actual name of doctor. 
	
	ajaxReq('GET', 'http://147.52.206.70:3000/GetPatients/'+"'"+doctorName+"'", null, getNumberOfPatients);
	//console.log(patientsNumber);
}

function LoadPatients(json){
    function callback(response){
        LoadPatientsPage(response);//Load Data of doctor.
    }
    callback(); //use this in order to load the next page automatically,if sign in data exchange works then remove.
    //var data = {"username" : "DocID" };   //add name from json argument.
    //ajax_request("POST",'Where to send - Add.',data,callback);
}

