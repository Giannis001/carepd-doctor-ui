(function(){
    var newPatient = new Vue({
        el: "#p1",
        data: {
            patient:
                {
                    image:'',
                    name: '',
                    surname: '',
                    gender: '',
                    age: ''
                }
            
        },
        methods: {
            addNewPatient(name, surname, gender, age, image){
                Vue.set(newPatient.patient, 'name', name)
                Vue.set(newPatient.patient, 'surname', surname)
                Vue.set(newPatient.patient, 'gender', gender)
                Vue.set(newPatient.patient, 'age', age)
                Vue.set(newPatient.patient, 'image', image)
            },
        },
        
        
    })
}());


function LoadCalendar(id){
    $('body').empty();
    $('body').append(
    '<div class="containter-fluid">'
+	    '<div class="row justify-content-center mt-2">'
+		    '<div class="col-2">'
+		        '<button onclick="GoBackFromCalendar()"class="topBtn btn backBtn">Back</button>'
+		    '</div>'
+		    '<div class="col-8"></div>'
+		    '<div class="col-2">'
+			    '<button onclick="GoToHome()"class="topBtn btn homeBtn">Home</button>'
+		    '</div>'
+	    '</div>'
+		'<div class="row justify-content-center">'
+           '<div class="col-3"></div>'
+			'<div class="col-3">'
+				'<label class="label" for="from">Start Date</label>'
+				'<br>'
+				'<input type="date" id="from" name="from" onchange="GetPatientsReqs('+id+')">'
+	        '</div>'
+			'<div class="col-3">'
+				'<label class="label" for="to">End Date</label>'
+				'<br>'
+				'<input type="date" id="to" name="to" onchange="GetPatientsReqs('+id+')">'
+			'</div>'
+           '<div class="col-3">'
+           '</div>'
+               '<div class="col-12 mt-4" id="warning"> </div>'
+	    '</div>'
+          '<div class="row justify-content-center" id="for_charts">'
+	      '</div>'
+	'</div>'
);

}