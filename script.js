
google.charts.load('current', {'packages':['corechart']});
google.charts.load('current', {'packages':['bar']});
//google.charts.setOnLoadCallback(drawChart);

function showChart(){
		
	var data2 = google.visualization.arrayToDataTable([
    ['Date', 'Hours'],
    [new Date(2020,3,01).toDateString(), 20],
    [new Date(2020,3,02).toDateString(), 17],
    [new Date(2020,3,03).toDateString(), 20],
    [new Date(2020,3,04).toDateString(), 16],
    [new Date(2020,3,05).toDateString(), 17],
    [new Date(2020,3,06).toDateString(), 14],
    [new Date(2020,3,07).toDateString(), 17.5],
    [new Date(2020,3,08).toDateString(), 11],
    [new Date(2020,3,09).toDateString(), 14],
    [new Date(2020,3,10).toDateString(), 10],
    [new Date(2020,3,11).toDateString(), 17],
    [new Date(2020,3,12).toDateString(), 15],
    [new Date(2020,3,13).toDateString(), 15],
    [new Date(2020,3,14).toDateString(), 12],
    [new Date(2020,3,15).toDateString(), 13],
    [new Date(2020,3,16).toDateString(), 10],
    [new Date(2020,3,17).toDateString(), 17],
    [new Date(2020,3,18).toDateString(), 10],
    [new Date(2020,3,19).toDateString(), 14],
    [new Date(2020,3,20).toDateString(), 13],
    [new Date(2020,3,21).toDateString(), 17],
    [new Date(2020,3,22).toDateString(), 10],
    [new Date(2020,3,23).toDateString(), 15],
    [new Date(2020,3,24).toDateString(), 10],
    [new Date(2020,3,25).toDateString(), 13],
    [new Date(2020,3,26).toDateString(), 13],
    [new Date(2020,3,27).toDateString(), 10],
    [new Date(2020,3,28).toDateString(), 16]]);

  var array3=[
    ['Date', 'Hours'],
    [new Date(2020,3,01).toDateString(),10],
    [new Date(2020,3,02).toDateString(), 13],
    [new Date(2020,3,03).toDateString(), 10],
    [new Date(2020,3,04).toDateString(), 14],
    [new Date(2020,3,05).toDateString(), 17],
    [new Date(2020,3,06).toDateString(), 14],
    [new Date(2020,3,07).toDateString(), 7],
    [new Date(2020,3,08).toDateString(), 11],
    [new Date(2020,3,09).toDateString(), 14],
    [new Date(2020,3,10).toDateString(), 10],
    [new Date(2020,3,11).toDateString(), 14],
    [new Date(2020,3,12).toDateString(), 5],
    [new Date(2020,3,13).toDateString(), 5],
    [new Date(2020,3,14).toDateString(), 12],
    [new Date(2020,3,15).toDateString(), 13],
    [new Date(2020,3,16).toDateString(), 11],
    [new Date(2020,3,17).toDateString(), 7],
    [new Date(2020,3,18).toDateString(), 10],
    [new Date(2020,3,19).toDateString(), 12],
    [new Date(2020,3,20).toDateString(), 13],
    [new Date(2020,3,21).toDateString(), 7],
    [new Date(2020,3,22).toDateString(), 10],
    [new Date(2020,3,23).toDateString(), 3],
    [new Date(2020,3,24).toDateString(), 10],
    [new Date(2020,3,25).toDateString(), 12],
    [new Date(2020,3,26).toDateString(), 3],
    [new Date(2020,3,27).toDateString(), 4],
    [new Date(2020,3,28).toDateString(), 6]];
          
  var max3= Math.max.apply(Math, array3.map(function (i) {
    return i[0][1];
  }));
	var data3 = google.visualization.arrayToDataTable(array3);
  var data4 = google.visualization.arrayToDataTable([
    ['Date', 'Hours'],
    [new Date(2020,3,01).toDateString(), 2],
    [new Date(2020,3,02).toDateString(), 1],
    [new Date(2020,3,03).toDateString(), 2],
    [new Date(2020,3,04).toDateString(), 0],
    [new Date(2020,3,05).toDateString(), 1],
    [new Date(2020,3,06).toDateString(), 1],
    [new Date(2020,3,07).toDateString(), 3],
    [new Date(2020,3,08).toDateString(), 2],
    [new Date(2020,3,09).toDateString(), 1],
    [new Date(2020,3,10).toDateString(), 0],
    [new Date(2020,3,11).toDateString(), 2],
    [new Date(2020,3,12).toDateString(), 1],
    [new Date(2020,3,13).toDateString(), 1],
    [new Date(2020,3,14).toDateString(), 2],
    [new Date(2020,3,15).toDateString(), 3],
    [new Date(2020,3,16).toDateString(), 1],
    [new Date(2020,3,17).toDateString(), 2],
    [new Date(2020,3,18).toDateString(), 1],
    [new Date(2020,3,19).toDateString(), 1],
    [new Date(2020,3,20).toDateString(), 0],
    [new Date(2020,3,21).toDateString(), 0],
    [new Date(2020,3,22).toDateString(), 2],
    [new Date(2020,3,23).toDateString(), 3],
    [new Date(2020,3,24).toDateString(), 1],
    [new Date(2020,3,25).toDateString(), 2],
    [new Date(2020,3,26).toDateString(), 2],
    [new Date(2020,3,27).toDateString(), 1],
    [new Date(2020,3,28).toDateString(), 1]]); 
            
  var data5 = google.visualization.arrayToDataTable([
    ['Date', 'Hours'],
    [new Date(2020,3,01).toDateString(), 4],
    [new Date(2020,3,02).toDateString(), 7],
    [new Date(2020,3,03).toDateString(), 6],
    [new Date(2020,3,04).toDateString(), 5],
    [new Date(2020,3,05).toDateString(), 9],
    [new Date(2020,3,06).toDateString(), 7],
    [new Date(2020,3,07).toDateString(), 8],
    [new Date(2020,3,08).toDateString(), 4],
    [new Date(2020,3,09).toDateString(), 2],
    [new Date(2020,3,10).toDateString(), 1],
    [new Date(2020,3,11).toDateString(), 1],
    [new Date(2020,3,12).toDateString(), 2],
    [new Date(2020,3,13).toDateString(), 3],
    [new Date(2020,3,14).toDateString(), 2],
    [new Date(2020,3,15).toDateString(), 4],
    [new Date(2020,3,16).toDateString(), 2],
    [new Date(2020,3,17).toDateString(), 5],
    [new Date(2020,3,18).toDateString(), 1],
    [new Date(2020,3,19).toDateString(), 1],
    [new Date(2020,3,20).toDateString(), 2],
    [new Date(2020,3,21).toDateString(), 2],
    [new Date(2020,3,22).toDateString(), 4],
    [new Date(2020,3,23).toDateString(), 3],
    [new Date(2020,3,24).toDateString(), 5],
    [new Date(2020,3,25).toDateString(), 2],
    [new Date(2020,3,26).toDateString(), 2],
    [new Date(2020,3,27).toDateString(), 1],
    [new Date(2020,3,28).toDateString(), 2]]); 

  var data6 = google.visualization.arrayToDataTable([
    ['Date', 'Hours'],
    [new Date(2020,3,01).toDateString(), 3],
    [new Date(2020,3,02).toDateString(), 2],
    [new Date(2020,3,03).toDateString(), 5],
    [new Date(2020,3,04).toDateString(), 2],
    [new Date(2020,3,05).toDateString(), 4],
    [new Date(2020,3,06).toDateString(), 7],
    [new Date(2020,3,07).toDateString(), 4],
    [new Date(2020,3,08).toDateString(), 7],
    [new Date(2020,3,09).toDateString(), 6],
    [new Date(2020,3,10).toDateString(), 3],
    [new Date(2020,3,11).toDateString(), 8],
    [new Date(2020,3,12).toDateString(), 6],
    [new Date(2020,3,13).toDateString(), 3],
    [new Date(2020,3,14).toDateString(), 4],
    [new Date(2020,3,15).toDateString(), 4],
    [new Date(2020,3,16).toDateString(), 2],
    [new Date(2020,3,17).toDateString(), 5],
    [new Date(2020,3,18).toDateString(), 6],
    [new Date(2020,3,19).toDateString(), 2],
    [new Date(2020,3,20).toDateString(), 3],
    [new Date(2020,3,21).toDateString(), 2],
    [new Date(2020,3,22).toDateString(), 4],
    [new Date(2020,3,23).toDateString(), 3],
    [new Date(2020,3,24).toDateString(), 2],
    [new Date(2020,3,25).toDateString(), 4],
    [new Date(2020,3,26).toDateString(), 6],
    [new Date(2020,3,27).toDateString(), 2],
    [new Date(2020,3,28).toDateString(), 2]
  ]);
          
	var data7 = new google.visualization.DataTable();
    data7.addColumn('string', 'Απαντήσεις που έδωσε στην ερώτηση "είστε καλά;"');
    data7.addColumn('number', 'Πόσες φορές έδωσε την κάθε απάντηση σε αυτό το διάστημα');
    data7.addRows([
      ['Καλά', 15],
		  ['Μέτρια', 17],
		  ['Κακά',5]
    ]);
		  
	var data8 = new google.visualization.DataTable();
    data8.addColumn('string', 'Απαντήσεις που έδωσε στην ερώτηση "γιατί δεν είστε καλά;"');
    data8.addColumn('number', 'Πόσες φορές έδωσε την κάθε απάντηση σε αυτό το διάστημα');
    data8.addRows([
      ['Δεν μπορώ να κουνηθώ', 5],
		  ['Έχω τρέμουλο', 4],
		  ['Έχω υπερκινησία', 3],
      ['Δεν αισθάνομαι καλά', 5]
    ]);
		
	var data9 = google.visualization.arrayToDataTable([
    ['Χρόνος', 'Είμαι καλά', 'Δεν είμαι καλά,\nδεν μπορώ να κουνηθώ', 'Δεν είμαι καλά,\nέχω τρέμουλο', 'Δεν είμαι καλά,\nέχω υπερκινησία','Δεν είμαι καλά,\nδεν αισθάνομαι καλά'],
    ['00:00-01:59', 10, 4, 2,3,1],
    ['02:00-03:59', 11, 4, 2,5,1],
    ['04:00-05:59', 6, 11, 3,1,0],
    ['06:00-07:59', 10, 5, 4,3,5],
		['08:00-09:59', 10, 4, 3,2,4],
    ['10:00-11:59', 10, 3, 3,5,0],
    ['12:00-13:59', 11, 4, 2,5,1],
    ['14:00-15:59', 6, 11, 3,1,0],
    ['16:00-17:59', 10, 5, 4,3,5],
    ['18:00-19:59', 10, 4, 3,2,4],
    ['20:00-21:59', 10, 3, 3,5,0],
    ['22:00-23:59', 10, 3, 3,5,0]
  ]);

  // Dummy Data
  var dataFromServer = [
	  [15.714285714286, 2.657296462534], // 16, 17, 14, 13, 18, 12, 20
	  [5.4285714285714, 1.9897697538834], // 6, 3, 9, 5, 7, 3, 5
	  [4.5714285714286, 2.2587697572631], // 6, 7, 8, 2, 3, 4, 2
	  [2.1428571428571, 0.98974331861079], // 2, 1, 3, 2, 2, 4, 1
	  [2.5714285714286, 1.1780301787479] // 3, 2, 5, 3, 2, 1, 2
  ];
		  
	
	drawBar1Chart(data2, 20);
  drawBar2Chart(data3, 17);
  drawBar3Chart(data4, 3);
  drawBar4Chart(data5, 9);
  drawBar5Chart(data6, 8);
  drawPie1Chart(data7);
	drawPie2Chart(data8);
  drawColumnChart(data9);
  drawErrorBarsChart(dataFromServer);
}

function drawBar1Chart(data, max){
  var max_normalize = Math.min((max+0.1*max), 24);
	var options = {
    title: 'Πόσες ώρες το 24ωρο ο ασθενής είναι ξύπνιος',
		legend: { position: 'none' },
		titleTextStyle:{bold:true,fontSize:20},
    hAxis: {
      title: 'Date',
      titleTextStyle:{italic:false,fontSize:20}
    },
    vAxis: {
      title: 'Hours',
      titleTextStyle:{italic:false,fontSize:20},
      minValue: 0,
      maxValue: max_normalize
    },
    chartArea: {height:'70%',width:'85%'},      
    backgroundColor: {
      stroke: '#2a2b2dff',
      strokeWidth: 3
    }
  };
  var chart = new google.visualization.ColumnChart(document.getElementById('bar1chart_div'));
  chart.draw(data, options);
}

function drawBar2Chart(data,max){
  var max_normalize = Math.min((max+0.1*max), 24);
	var options = {
    title: 'Πόσες ώρες την ημέρα ο ασθενής περπατάει / κινείται άνετα / είναι καλά',
		legend: { position: 'none'},
		titleTextStyle:{bold:true,fontSize:20},
    hAxis: {
      title: 'Date',
      titleTextStyle:{italic:false,fontSize:20}
    },
    vAxis: {
      title: 'Hours',
      titleTextStyle:{italic:false,fontSize:20},
		  minValue: 0,
      maxValue: max_normalize,
    },
    chartArea: {height:'70%',width:'85%'},
		backgroundColor: {
      stroke: '#2a2b2dff',
      strokeWidth: 5
    }
  };
  var chart = new google.visualization.ColumnChart(document.getElementById('bar2chart_div'));
  chart.draw(data, options);
}
    
function drawBar3Chart(data, max){
  var max_normalize = Math.min((max+0.1*max), 24);
	var options = {
    title: 'Πόσες ώρες την ημέρα, ενώ είναι ξύπνιος, ο ασθενής δεν μπορεί να περπατήσει καθόλου',
		legend: { position: 'none' },
		titleTextStyle:{bold:true,fontSize:20},
    hAxis: {
      title: 'Date',
      titleTextStyle:{italic:false,fontSize:20}
    },
    vAxis: {
      title: 'Hours',
      titleTextStyle:{italic:false,fontSize:20},
		  minValue: 0,
		  maxValue: max_normalize
    },
    chartArea: {height:'70%',width:'85%'},
    backgroundColor: {
      stroke: '#2a2b2dff',
      strokeWidth: 5
    }
  };
  var chart = new google.visualization.ColumnChart(document.getElementById('bar3chart_div'));
  chart.draw(data, options);
}
    
function drawBar4Chart(data,max){
  var max_normalize = Math.min((max+0.1*max), 24);
	var options = {
    title: 'Πόσες ώρες την ημέρα ο ασθενής περπατάει αργά και δύσκολα',
		legend: { position: 'none' },
		titleTextStyle:{bold:true,fontSize:20},
    hAxis: {
      title: 'Date',
      titleTextStyle:{italic:false,fontSize:20}
    },
    vAxis: {
      title: 'Hours',
      titleTextStyle:{italic:false,fontSize:20},
		  minValue: 0,
		  maxValue: max_normalize
    },
    chartArea: {height:'70%',width:'85%'},
    backgroundColor: {
      stroke: 'black',
      strokeWidth: 5
    }
  };
  var chart = new google.visualization.ColumnChart(document.getElementById('bar4chart_div'));
  chart.draw(data, options);
}
    
function drawBar5Chart(data, max){
  var max_normalize = Math.min((max+0.1*max), 24);
	var options = {
    title: 'Πόσες ώρες την ημέρα ο ασθενής εμφανίζει παραπάνω κινητικότητα από το φυσιολογικό',
		legend: { position: 'none' },
		titleTextStyle:{bold:true,fontSize:20},
    hAxis: {
      title: 'Date',
      titleTextStyle:{italic:false,fontSize:20}
    },
    vAxis: {
      title: 'Hours',
      titleTextStyle:{italic:false,fontSize:20},
		  minValue: 0,
		  maxValue: max_normalize
    },
    chartArea: {
      height:'70%',
      width:'85%',
      backgroundColor: {
        stroke: '#2a2b2dff',
        strokeWidth: 3
      }
    },
    backgroundColor: {
      stroke: '#2a2b2dff',
      strokeWidth: 3
    },
  };
  var chart = new google.visualization.ColumnChart(document.getElementById('bar5chart_div'));
  chart.draw(data, options);
}
	  
function drawPie1Chart(data) {
  var options = {'title':'Πως απάντησε ο ασθενής στην ερώτηση "Πως αισθάνεστε τώρα;"',
    titleTextStyle:{bold:true,fontSize:20},
	  is3D: true,
    chartArea:{height:'70%',width:'79%',left:50,top:100},
    backgroundColor: {
      stroke: '#2a2b2dff',
      strokeWidth: 5
    },
		legend:{
      position: 'right',alignment:'center', 
			textStyle: {italic:false, fontSize: 18}
		},
		pieSliceTextStyle:{fontSize: 20},
		colors: ['#109618','#e8711a','#ff2e2f']
  };

  // Instantiate and draw our chart, passing in some options.
  var chart = new google.visualization.PieChart(document.getElementById('pie1chart_div'));
  chart.draw(data, options);
}
	  
function drawPie2Chart(data) {
  var options = {title:'Πως απάντησε ο ασθενής στην ερώτηση "Γιατί δεν είστε καλά;"',
    titleTextStyle:{bold:true,fontSize:20},
		is3D: true,
    chartArea:{height:'75%',width:'79%',left:50,top:100},
    backgroundColor: {
      stroke: '#2a2b2dff',
      strokeWidth: 5
    },
		legend:{position: 'right',alignment:'center', 
		  textStyle: {italic:false, fontSize: 18}
		},
    pieSliceTextStyle:{fontSize: 20},
    colors: ['#ff2e2f', '#e8711a', '#ddb32a', '#0054a6']
  };
  //Instantiate and draw our chart, passing in some options.
  var chart = new google.visualization.PieChart(document.getElementById('pie2chart_div'));
  chart.draw(data, options);
}
	  
function drawColumnChart(data) {
  var options = {
    title: 'Πώς απάντησε ο ασθενής στο επιλεγμένο χρονικό διάστημα.',
    legend:{
      position:'left', alignment:'center',
      textStyle: {italic:false, fontSize: 20,color:'#000'}
    },
    titleTextStyle:{bold:false,fontSize:25,color:'#000'},
    hAxis: {
      title: 'Time',
      titleTextStyle:{italic:false,fontSize:20}
    },
    vAxis:{
      titleTextStyle:{italic:false,fontSize:20}
    },
    chartArea:{
      height:'75%',
      width:'85%',
    },
    backgroundColor: {
      stroke: '#2a2b2dff',
      strokeWidth: 3
    },
    
    colors: ['#109618','#ff2e2f', '#e8711a', '#ddb32a', '#0054a6']
  };
  var chart = new google.charts.Bar(document.getElementById('columnchart_material'));
  chart.draw(data, google.charts.Bar.convertOptions(options));
}

function drawErrorBarsChart(dataFromServer) {
	var data = new google.visualization.DataTable();
	var questions = [	// Hard coded, could be received from server but not necessary right now
		'Πόσες ώρες το 24ωρο ο ασθενής είναι ξύπνιος',
		'Πόσες  ώρες  την ημέρα ο ασθενής  περπατάει/κινείται άνετα /είναι καλά',
		'Πόσες ώρες την ημέρα δεν μπορεί να περπατήσει καθόλου ενώ είναι ξύπνιος',
		'Πόσες ώρες την ημέρα περπατάει αργά και δύσκολα',
		'Πόσες ώρες την ημέρα εμφανίζει παραπάνω κινητικότητα από το φυσιολογικό'
	];

	var array = questions;
	for (var i = 0; i < questions.length; i++) {	// Calculate min, max
		array[i] = ([questions[i], dataFromServer[i][0], dataFromServer[i][0] + 
		dataFromServer[i][1], dataFromServer[i][0] - dataFromServer[i][1]]);
	}

	data.addColumn('string', 'Question');
	data.addColumn('number', 'Mean');
	data.addColumn({
		id: 'max',
		type: 'number',
		role: 'interval'
	});
	data.addColumn({
		id: 'min',
		type: 'number',
		role: 'interval'
	});
	data.addRows(array);

	var options_bars = {
		title: 'Error Bars & Confidence Intervals',
		series: [{
			'color': '#0054a6'
    }],
    chartArea:{
      height: '75%',
      width: '70%',
      left: '25%'
    },
		bar: {
			groupWidth: "30%"
		},
		textPosition: 'out',
		fontSize: 20,
		Mean: {
			lineWidth: 2,
			style: 'boxes',
			color: '#FF0000'
		},
		interval: {
			max: {
				style: 'bars',
				color: 'FF0000'
			},
			min: {
				style: 'bars',
				color: 'FF0000'
			}
		},
    legend: 'none',
    backgroundColor: {
      stroke: '#2a2b2dff',
      strokeWidth: 3
    } 
	};

	var chart_lines = new google.visualization.BarChart(document.getElementById('chart_lines'));
	chart_lines.draw(data, options_bars);
}

function ajaxReq(method, link, data, callback){
	var xhr = new XMLHttpRequest();
	var response;
	xhr.onload = function () {
		if(xhr.readyState === 4 && xhr.status === 200){
			response=JSON.parse(xhr.responseText);
			callback(response);
		}
		else if(xhr.status !== 200){
			alert('Request failed. Returned status of ' + xhr.status);
		}
	};
	if(data===null)
		xhr.open(method, link);
	else
		xhr.open(method, link, data);
	xhr.send();
}

//A function "standardDeviation" which will take an array of counts and will calculate the deviation (the data that ErrorBarsChart need).

const standardDeviation = (arr, usePopulation = true) => {
  const mean = arr.reduce((acc, val) => acc + val, 0) / arr.length;
  return Math.sqrt(
    arr.reduce((acc, val) => acc.concat((val - mean) ** 2), []).reduce((acc, val) => acc + val, 0) /
      (arr.length - (usePopulation ? 0 : 1))
  );
};

//A function "calculateAverage" which will take an array of counts and will calculate the mean (the data that ErrorBarsChart need).

function calculateAverage(counts){
  var sum=0;
  for(i=0;i<counts.length;i++){
    sum+=counts[i];
  }
  return sum/(counts.length);
}

//Global variable: An array which contains pairs (mean and deviation of an array of counts). We need that for the ErrorBarsChart and for correct sychronization.
var dataToProcessForErrorBarsChart=[];

function restartDataToProcessForErrorBarsChart(){
    for(i=0;i<5;i++){ //5 theseis theloume ston pinaka. Den vazoume 'data.length' giati sthn arxh den einai arxikopoihmenos o pinakas.
      dataToProcessForErrorBarsChart[i]=[-1,-1];
    }
}

function errorBarsDataComplete(){
  for(i=0;i<dataToProcessForErrorBarsChart.length;i++){
    if(dataToProcessForErrorBarsChart[i]==[-1,-1]){
      return 0;
    }
  }
  return 1;
}

patient_List = [];

function GetPatientHoursAwakeCallBack(patient){
  console.log(patient);
  patient_List[0] = patient;

  //If we dont have data for this chart.
if(patient==null || patient.length==0){
  var pairTest=[];
  pairTest.push(0,0);
  dataToProcessForErrorBarsChart[0]=pairTest;
  var data=[];
    data.push(['Date', 'Hours']);
    data.push(["",0]);
    var data2 = google.visualization.arrayToDataTable(data);
    drawBar1Chart(data2,0.1);
    if(errorBarsDataComplete() ==1){
      drawErrorBarsChart(dataToProcessForErrorBarsChart);
    }
    return;
}

  var max=-1;
  //Prepare data
  var arrayCountsForErrorBars=[];
  var pairsMeanAndDeviation=[];
  var data=[];
  data.push(['Date', 'Hours']);
  for(i=0;i<patient.length;i++){
    var tmpDate=new Date(patient[i].date);
    var strDate=tmpDate.getDate()+"/"+(tmpDate.getMonth()+1)+"/"+tmpDate.getFullYear();
    data.push([strDate,patient[i].count]);
    //Create an array with the count data of this patient for the specific question and give it to the ErrorBarChart.
    arrayCountsForErrorBars.push(patient[i].count);
     //Find max
	if(max<patient[i].count){
		max=patient[i].count;
	}
  }

  //Process and give data for ErrorBarsChart
  pairsMeanAndDeviation.push(calculateAverage(arrayCountsForErrorBars),standardDeviation(arrayCountsForErrorBars));
  dataToProcessForErrorBarsChart[0]=pairsMeanAndDeviation;
  var data2 = google.visualization.arrayToDataTable(data);
  drawBar1Chart(data2, max);
  //If first five charts have been drawn then draw the ErrorBarChart. Maybe unnecessary check.
  if(errorBarsDataComplete() ==1){
    drawErrorBarsChart(dataToProcessForErrorBarsChart);
  }
}

function GetPatientIsWellHoursCallBack(patient){
  console.log(patient);
//Otan mpei to hoursAwake auto na fugei!!
var test=[1, 0.5, 2, 2];
var pairsTest=[];
pairsTest.push(calculateAverage(test),standardDeviation(test));
dataToProcessForErrorBarsChart[0]=pairsTest;
patient_List[0] = patient;
//----------------------------------------
  
  patient_List[1] = patient;

//If we dont have data for this chart.
if(patient==null || patient.length==0){
  var pairTest=[];
  pairTest.push(0,0);
  dataToProcessForErrorBarsChart[1]=pairTest;
  var data=[];
    data.push(['Date', 'Hours']);
    data.push(["",0]);
    var data2 = google.visualization.arrayToDataTable(data);
    drawBar2Chart(data2,0.1);
    if(errorBarsDataComplete() ==1){
      drawErrorBarsChart(dataToProcessForErrorBarsChart);
    }
    return;
}

  var max=-1;
//Prepare data
  //var arrayCountsForErrorBars=[6, 3, 9, 5, 7, 3, 5];
  var arrayCountsForErrorBars=[];
  var pairsMeanAndDeviation=[];
  var data=[];
  data.push(['Date', 'Hours']);
  for(i=0;i<patient.length;i++){
    var tmpDate=new Date(patient[i].date);
    var strDate=tmpDate.getDate()+"/"+(tmpDate.getMonth()+1)+"/"+tmpDate.getFullYear();
    data.push([strDate,patient[i].count]);
//Create an array with the count data of this patient for the specific question and give it to the ErrorBarChart.
    arrayCountsForErrorBars.push(patient[i].count);
    //Find max
    if(max<patient[i].count){
        max=patient[i].count;
    }
  }

  //Process and give data for ErrorBarsChart
  pairsMeanAndDeviation.push(calculateAverage(arrayCountsForErrorBars),standardDeviation(arrayCountsForErrorBars));
  dataToProcessForErrorBarsChart[1]=pairsMeanAndDeviation;
  var data2 = google.visualization.arrayToDataTable(data);
  drawBar2Chart(data2, max);
  //If first five charts have been drawn then draw the ErrorBarChart. Maybe unnecessary check.
  if(errorBarsDataComplete() ==1){
    drawErrorBarsChart(dataToProcessForErrorBarsChart);
  }
}

function GetPatientNotWalkingHoursCallBack(patient){
  console.log(patient);
  patient_List[2] = patient;
   //If we dont have data for this chart.
if(patient==null || patient.length==0){
  var pairTest=[];
  pairTest.push(0,0);
  dataToProcessForErrorBarsChart[2]=pairTest;
  var data=[];
    data.push(['Date', 'Hours']);
    data.push(["",0]);
    var data2 = google.visualization.arrayToDataTable(data);
    drawBar3Chart(data2,0.1);
    if(errorBarsDataComplete() ==1){
      drawErrorBarsChart(dataToProcessForErrorBarsChart);
    }
    return;
}

  var max=-1;
  //Prepare data
  //var arrayCountsForErrorBars=[6, 7, 8, 2, 3, 4, 2];
  var arrayCountsForErrorBars=[];
  var pairsMeanAndDeviation=[];
  var data=[];
  data.push(['Date', 'Hours']);
  for(i=0;i<patient.length;i++){
    var tmpDate=new Date(patient[i].date);
    var strDate=tmpDate.getDate()+"/"+(tmpDate.getMonth()+1)+"/"+tmpDate.getFullYear();
    data.push([strDate,patient[i].count]);
    //Create an array with the count data of this patient for the specific question and give it to the ErrorBarChart.
    arrayCountsForErrorBars.push(patient[i].count);
     //Find max
     if(max<patient[i].count){
      max=patient[i].count;
  }
  }

  //Process and give data for ErrorBarsChart
  pairsMeanAndDeviation.push(calculateAverage(arrayCountsForErrorBars),standardDeviation(arrayCountsForErrorBars));
  dataToProcessForErrorBarsChart[2]=pairsMeanAndDeviation;
  var data2 = google.visualization.arrayToDataTable(data);
    drawBar3Chart(data2, max);

    //If first five charts have been drawn then draw the ErrorBarChart.
    if(errorBarsDataComplete() ==1){
      drawErrorBarsChart(dataToProcessForErrorBarsChart);
    }
}

function GetPatientSlowAndDifficultWalkingHoursCallBack(patient){
  console.log(patient);
  patient_List[3] = patient;

//If we dont have data for this chart.
if(patient==null || patient.length==0 ){
  var pairTest=[];
  pairTest.push(0,0);
  dataToProcessForErrorBarsChart[3]=pairTest;
  var data=[];
    data.push(['Date', 'Hours']);
    data.push(["",0]);
    var data2 = google.visualization.arrayToDataTable(data);
    drawBar4Chart(data2,0.1);
    if(errorBarsDataComplete() ==1){
      drawErrorBarsChart(dataToProcessForErrorBarsChart);
    }
    return;
}

  var max=-1;
  //Prepare data
  //var arrayCountsForErrorBars=[ 2, 1, 3, 2, 2, 4, 1];
  var arrayCountsForErrorBars=[];
  var pairsMeanAndDeviation=[];
  var data=[];
  data.push(['Date', 'Hours']);
  for(i=0;i<patient.length;i++){
    var tmpDate=new Date(patient[i].date);
    var strDate=tmpDate.getDate()+"/"+(tmpDate.getMonth()+1)+"/"+tmpDate.getFullYear();
    data.push([strDate,patient[i].count]);
    //Create an array with the count data of this patient for the specific question and give it to the ErrorBarChart.
    arrayCountsForErrorBars.push(patient[i].count);
     //Find max
     if(max<patient[i].count){
      max=patient[i].count;
  }
  }

  //Process and give data for ErrorBarsChart
  pairsMeanAndDeviation.push(calculateAverage(arrayCountsForErrorBars),standardDeviation(arrayCountsForErrorBars));
  dataToProcessForErrorBarsChart[3]=pairsMeanAndDeviation;
  var data2 = google.visualization.arrayToDataTable(data);
    drawBar4Chart(data2, max);
    
    //If first five charts have been drawn then draw the ErrorBarChart. Maybe unnecessary check.
    if(errorBarsDataComplete() ==1){
      drawErrorBarsChart(dataToProcessForErrorBarsChart);
    }
}

function GetPatientHyperactivityHoursCallBack(patient){
  console.log(patient);
  patient_List[4] = patient;

//If we dont have data for this chart.
if(patient==null || patient.length==0){
  var pairTest=[];
  pairTest.push(0,0);
  dataToProcessForErrorBarsChart[4]=pairTest;
  var data=[];
    data.push(['Date', 'Hours']);
    data.push(["",0]);
    var data2 = google.visualization.arrayToDataTable(data);
    drawBar5Chart(data2,0.1);
    if(errorBarsDataComplete() ==1){
      drawErrorBarsChart(dataToProcessForErrorBarsChart);
    }
    return;
}

  var max=-1;
  //Prepare data
  //var arrayCountsForErrorBars=[ 3, 2, 5, 3, 2, 1, 2];
  var arrayCountsForErrorBars=[];
  var pairsMeanAndDeviation=[];
  var data=[];

  data.push(['Date', 'Hours']);
  for(i=0;i<patient.length;i++){
    var tmpDate=new Date(patient[i].date);
    var strDate=tmpDate.getDate()+"/"+(tmpDate.getMonth()+1)+"/"+tmpDate.getFullYear();
    data.push([strDate,patient[i].count]);
    //Create an array with the count data of this patient for the specific question and give it to the ErrorBarChart.
    arrayCountsForErrorBars.push(patient[i].count);
     //Find max
    if(max<patient[i].count){
      max=patient[i].count;
    }
  }

  //Process and give data for ErrorBarsChart
  pairsMeanAndDeviation.push(calculateAverage(arrayCountsForErrorBars),standardDeviation(arrayCountsForErrorBars));
  dataToProcessForErrorBarsChart[4]=pairsMeanAndDeviation;
  var data2 = google.visualization.arrayToDataTable(data);
    drawBar5Chart(data2, max);
    //If first five charts have been drawn then draw the ErrorBarChart. Maybe unnecessary check.
    if(errorBarsDataComplete() ==1){
      drawErrorBarsChart(dataToProcessForErrorBarsChart);
    }
}

//===================================
$( window ).resize(function() {
	clearTimeout($.data(this, 'resizeTimer'));
    $.data(this, 'resizeTimer', setTimeout(function() {
	   if(patient_List.length==5){
		   GetPatientHoursAwakeCallBack(patient_List[0]);
		   GetPatientNotWalkingHoursCallBack(patient_List[1]);
		   GetPatientIsWellHoursCallBack(patient_List[2]);
		   GetPatientSlowAndDifficultWalkingHoursCallBack(patient_List[3]);
		   GetPatientHyperactivityHoursCallBack(patient_List[4]);
	   }
		  
	}, 200));
});
//===================================

function GetPatientsReqs(id){
    var fromDate = document.getElementById('from');
    var toDate = document.getElementById('to');
  var patientid = id;
  
  $('#warning').empty();
  $('#for_charts').empty();
  
 if (fromDate.value=='' || toDate.value==''){
   return;
 }
  
	if(fromDate.value > toDate.value){

    $('#warning').append(
        ' <div class="alert alert-warning">'
+'  <strong> Warning!</strong> Invalid dates. Please check dates again.'
+'</div>');

   return; 
  }

  $('#for_charts').append(
          '<div class="col-1"></div>'
+           '<div class="col-10 pt-5 pb-5">'
+               '<div id="contain_chart">'
+	            '<div id="bar1chart_div" class="charts"></div>'
+               '</div>'
+	        '</div>'
+	        '<div class="col-1"></div>'
+           '<div class="col-1"></div>'
+	        '<div class="col-10  pt-5 pb-5">'
+               '<div id="contain_chart">'
+	            '<div id="bar2chart_div" class="charts"></div>'
+               '</div>'
+	        '</div>'
+           '<div class="col-1"></div>'
+           '<div class="col-1"></div>'
+	        '<div class="col-10 pt-5 pb-5">'
+               '<div id="contain_chart">'
+	            '<div id="bar3chart_div" class="charts"></div>'
+               '</div>'
+	        '</div>'
+	        '<div class="col-1"></div>'
+           '<div class="col-1"></div>'
+	        '<div class="col-10  pt-5 pb-5">'
+               '<div id="contain_chart">'
+	            '<div id="bar4chart_div" class="charts"></div>'
+               '</div>'
+	        '</div>'
+	        '<div class="col-1"></div>'
+           '<div class="col-1"></div>'
+	        '<div class="col-10 pt-5 pb-5">'
+               '<div id="contain_chart">'
+	            '<div id="bar5chart_div" class="charts"></div>'
+               '</div>'
+	        '</div>'
+	        '<div class="col-1"></div>'
+           '<div class="col-1"></div>'
+	        '<div class="col-5 pt-5 pb-5">'
+               '<div id="contain_chart">'
+		        '<div id="pie1chart_div" class="charts"></div>'
+               '</div>'
+	        '</div>'
+	        '<div class="col-5 pt-5 pb-5">'
+               '<div id="contain_chart">'
+		        '<div id="pie2chart_div" class="charts"></div>'
+               '</div>'
+	        '</div>'
+	        '<div class="col-1"></div>'
+           '<div class="col-1"></div>'
+	        '<div class="col-10 pt-5 pb-5">'
+               '<div id="contain_chart">'
+	    	        '<div id="columnchart_material" class="charts"></div>'
+               '</div>'
+           '</div>'
+	        '<div class="col-1"></div>'
+           '<div class="col-1"></div>'
+	        '<div class="col-10 pt-5 pb-5">'
+               '<div id="contain_chart">'
+		        '<div id="chart_lines" class="charts"></div>'
+               '</div>'
+	        '</div>'
+           '<div class="col-1"></div>'
  );

	restartDataToProcessForErrorBarsChart(); //makes pairs of data [-1,-1] for the check.
	patient_List.length = 0;
	console.log(patient_List);
	//-----------------TODO replace localhost:3000 with Server's IP and PORT-----------------
	//DON'T SEND THIS REQUEST!!!! ajaxReq('GET', 'http://147.52.206.70:3000/GetPatientHoursAwake'+'/' + patientid +'/'+ "'" + fromDate.value + "'" +'/' + "'" + toDate.value + "'", null, GetPatientHoursAwakeCallBack);
	ajaxReq('GET', 'http://147.52.206.70:3000/GetPatientIsWellHours'+'/' + patientid +'/'+ "'" + fromDate.value + "'" +'/' + "'" + toDate.value + "'", null, GetPatientIsWellHoursCallBack);
	ajaxReq('GET', 'http://147.52.206.70:3000/GetPatientNotWalkingHours'+'/' + patientid +'/'+ "'" + fromDate.value + "'" +'/' + "'" + toDate.value + "'", null, GetPatientNotWalkingHoursCallBack);
	ajaxReq('GET', 'http://147.52.206.70:3000/GetPatientSlowAndDifficultWalkingHours'+'/' + patientid +'/'+ "'" +fromDate.value + "'" +'/' + "'" + toDate.value + "'", null, GetPatientSlowAndDifficultWalkingHoursCallBack);
	ajaxReq('GET', 'http://147.52.206.70:3000/GetPatientHyperactivityHours'+'/'+patientid+'/'+ "'" + fromDate.value + "'" +'/' + "'" + toDate.value + "'", null, GetPatientHyperactivityHoursCallBack);
 
}